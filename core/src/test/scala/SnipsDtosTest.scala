import info.acidflow.homer.dto.snips.SnipsMessage
import io.circe.generic.auto._
import io.circe.parser._
import org.scalatest.FlatSpec
class SnipsDtosTest extends FlatSpec {
  it should "parse a dto" in {
    val json = """
      {"sessionId":"33facc75-335d-4fde-bb93-a23a4a30df06","customData":null,"siteId":"default","input":"what 's the weather on flag","intent":{"intentName":"searchWeatherForecast","probability":0.886036},"slots":[]}
    """

    println(decode[SnipsMessage](json))
  }

  it should "parse a dto 2" in {
    val json = """
      {"sessionId":"33facc75-335d-4fde-bb93-a23a4a30df06","customData":null,"siteId":"default",
 | "input": "weather in paris tomorrow",
 |  "intent": {
 |    "intentName": "searchWeatherForecast",
 |    "probability": 0.88317275
 |  },
 |  "slots": [
 |    {
 |      "rawValue": "paris",
 |      "value": {
 |        "kind": "Custom",
 |        "value": "Paris"
 |      },
 |      "range": {
 |        "start": 11,
 |        "end": 16
 |      },
 |      "entity": "locality",
 |      "slotName": "forecast_locality"
 |    },
 |    {
 |      "rawValue": "tomorrow",
 |      "value": {
 |        "kind": "InstantTime",
 |        "value": "2018-05-31 00:00:00 +00:00",
 |        "grain": "Day",
 |        "precision": "Exact"
 |      },
 |      "range": {
 |        "start": 17,
 |        "end": 25
 |      },
 |      "entity": "snips/datetime",
 |      "slotName": "forecast_start_datetime"
 |    }
 |  ]
 |}""".stripMargin

    println(decode[SnipsMessage](json))
  }
}
