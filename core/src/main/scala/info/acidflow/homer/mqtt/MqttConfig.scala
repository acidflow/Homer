package info.acidflow.homer.mqtt

import com.typesafe.config.Config

object MqttConfig {

  def apply(config: Config): MqttConfig = {
    MqttConfig(
      config.getString("mqtt.address"),
      config.getInt("mqtt.port")
    )
  }

}

case class MqttConfig(address: String, port: Int) {

  def uri = s"tcp://$address:$port"
  
}
