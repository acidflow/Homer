package info.acidflow.homer

import info.acidflow.homer.modules.{AkkaModule, ConfigModule, MqttModule}

trait SnipsApp extends App with AkkaModule with MqttModule with ConfigModule