package info.acidflow.homer.stream

import akka.stream.Supervision
import com.typesafe.scalalogging.LazyLogging

object LogResumeDecider extends LazyLogging {

  val decider: Supervision.Decider = {
    case e: Exception =>
      logger.error("An exception occurred", e)
      Supervision.Resume
    case _ => Supervision.Stop
  }

}
