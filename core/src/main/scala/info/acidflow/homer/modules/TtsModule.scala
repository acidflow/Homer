package info.acidflow.homer.modules

import akka.actor.ActorRef
import info.acidflow.homer.tts.TTSActor

trait TtsModule {
  self : AkkaModule with MqttModule =>

  val ttsActor: ActorRef = system.actorOf(TTSActor.props(mqttSink), "tts")

}
