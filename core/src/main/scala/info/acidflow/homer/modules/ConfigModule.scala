package info.acidflow.homer.modules

import com.typesafe.config.Config

trait ConfigModule {
  val config : Config
}
