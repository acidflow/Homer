package info.acidflow.homer.modules

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContext

trait AkkaModule {
  implicit val system : ActorSystem
  implicit val executionContext : ExecutionContext
  implicit val materializer : ActorMaterializer
}
