package info.acidflow.homer.modules

import akka.Done
import akka.stream.alpakka.mqtt.scaladsl.{MqttSink, MqttSource}
import akka.stream.alpakka.mqtt.{MqttConnectionSettings, MqttMessage, MqttQoS, MqttSourceSettings}
import akka.stream.scaladsl.Source

import scala.concurrent.Future

trait MqttModule {
  self : ConfigModule =>

  val connectionSettings : MqttConnectionSettings
  val subscriptions : Map[String, MqttQoS]
  val mqttSource: Source[MqttMessage, Future[Done]] = MqttSource.atMostOnce(
    MqttSourceSettings(connectionSettings, subscriptions), bufferSize = 8
  )
  val mqttSink = MqttSink(connectionSettings, MqttQoS.AtLeastOnce)

}
