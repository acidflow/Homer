package info.acidflow.homer.tts

import akka.Done
import akka.actor.{Actor, ActorLogging, Props}
import akka.stream.alpakka.mqtt.{MqttMessage, MqttQoS}
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import akka.util.ByteString
import info.acidflow.homer.dto.snips.SnipsSayText
import info.acidflow.homer.tts.TTSActor.SayMsg
import io.circe.generic.auto._
import io.circe.syntax._

import scala.concurrent.Future

object TTSActor {

  def props(sink: Sink[MqttMessage, Future[Done]]): Props = Props(new TTSActor(sink))

  case class SayMsg(snipsDto: SnipsSayText)

}


class TTSActor(val sink: Sink[MqttMessage, Future[Done]]) extends Actor with ActorLogging {

  implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))

  override def receive: Receive = {
    case SayMsg(snipsText) =>
      log.info(s"TTS received : $snipsText")
      Source.single(
        MqttMessage("hermes/tts/say", ByteString(snipsText.asJson.toString()), Some(MqttQoS.AtLeastOnce))
      ).runWith(sink)
  }
}

