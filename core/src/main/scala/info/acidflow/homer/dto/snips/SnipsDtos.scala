package info.acidflow.homer.dto.snips

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import io.circe.generic.auto._
import io.circe.java8.time.decodeLocalDateTime
import io.circe.{Decoder, DecodingFailure}

sealed trait SlotValue

object SlotValue {

  implicit val decodeLocalDateTimeSnipsFormat: Decoder[LocalDateTime] = decodeLocalDateTime(
    DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH:mm:ss' 'xxx")
  )

  implicit val decodeSlotValue: Decoder[SlotValue] = Decoder.instance(c =>
    c.downField("kind").as[String].flatMap {
      case "Custom" => c.as[CustomSlotValue]
      case "InstantTime" => c.as[InstantTimeSlotValue]
      case "TimeInterval" => c.as[TimeIntervalSlotValue]
      case k: String => Left(DecodingFailure(s"Unknown kind $k", c.history))
    }
  )
}

case class SnipsMessage(sessionId: String, customData: Option[String], siteId: String, input: String, intent: Intent, slots: Seq[Slot])

case class Intent(intentName: String, probability: Double)

case class Slot(rawValue: String, value: SlotValue, range: Range, entity: String, slotName: String)

case class CustomSlotValue(value: String) extends SlotValue

case class InstantTimeSlotValue(value: LocalDateTime, grain: String, precision: String) extends SlotValue

case class TimeIntervalSlotValue(from: LocalDateTime, to: LocalDateTime) extends SlotValue

case class Range(start: Long, end: Long)

case class SnipsSayText(text: String, siteId: String = "default")
