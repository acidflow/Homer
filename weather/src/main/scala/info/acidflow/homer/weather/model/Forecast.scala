package info.acidflow.homer.weather.model

import java.time.LocalDateTime

case class Forecast(cityName: String, weather: String, temperature: Double, date: LocalDateTime)
