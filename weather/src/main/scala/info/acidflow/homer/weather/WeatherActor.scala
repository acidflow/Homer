package info.acidflow.homer.weather

import java.time.format.DateTimeFormatter
import java.util.Locale

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Status}
import akka.pattern.pipe
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import info.acidflow.homer.dto.snips.{InstantTimeSlotValue, SnipsMessage, SnipsSayText, TimeIntervalSlotValue}
import info.acidflow.homer.tts.TTSActor.SayMsg
import info.acidflow.homer.weather.WeatherActor.ForecastMessage
import info.acidflow.homer.weather.api.OpenWeatherApi
import info.acidflow.homer.weather.model.Forecast
import info.acidflow.homer.weather.model.dto.Slots

import scala.concurrent.ExecutionContext


object WeatherActor {

  def props(ttsActor: ActorRef, api: OpenWeatherApi): Props = {
    Props(new WeatherActor(ttsActor, api))
  }

  case class ForecastMessage(forecast: Forecast)

}

class WeatherActor(val ttsActor: ActorRef, val api: OpenWeatherApi) extends Actor with ActorLogging {

  implicit val ec: ExecutionContext = context.dispatcher
  implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))
  var maybeCurrentMessage: Option[SnipsMessage] = None

  private def searchWeatherForecast(intent: SnipsMessage) = {
    context.become(receiveHttpSearchResponse)

    val locality = intent.slots.find(_.slotName == Slots.forecastLocality).map(_.rawValue)
    val startDate = intent.slots.find(_.slotName == Slots.forecastStartDatetime)
      .map(_.value).collect {
      case InstantTimeSlotValue(date, _, _) => date
      case TimeIntervalSlotValue(from, _) => from
    }

    api.forecast(locality, startDate).map(ForecastMessage).pipeTo(self)
  }

  def receive: Receive = {
    case msg: SnipsMessage =>
      maybeCurrentMessage = Option(msg)
      msg.intent.intentName match {
        case "searchWeatherForecast" => searchWeatherForecast(msg)
      }

    case wtf =>
      log.info(s"Received unknown message, $wtf")
  }

  private def receiveHttpSearchResponse: Receive = handleForecastMessage.andThen(_ => {
    context.unbecome()
    maybeCurrentMessage = null
  })

  private def handleForecastMessage: Receive = {
    case ForecastMessage(forecast) =>
      ttsActor ! SayMsg(
        SnipsSayText(
          s"""Forecast for ${forecast.cityName} on
             |${forecast.date.toLocalDate.format(DateTimeFormatter.ofPattern("EEEE d MMMM", Locale.ROOT))}:
             |${forecast.weather}, ${forecast.temperature} degrees.
             |""".stripMargin,
          extractSiteId(maybeCurrentMessage)
        )
      )

    case Status.Failure(e) =>
      log.error("Error while getting weather forecast, response {}", e)

      ttsActor ! SayMsg(
        SnipsSayText("I am sorry, I could not get the weather forecast.", extractSiteId(maybeCurrentMessage))
      )
  }

  def extractSiteId(msg: Option[SnipsMessage]) = {
    maybeCurrentMessage.map(_.siteId).getOrElse("default")
  }

}