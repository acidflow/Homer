package info.acidflow.homer.weather.api.dto

// Common objects

case class Coordinates(lon: Double, lat: Double)

case class Weather(id: Int, main: String, description: String, icon: String)

case class MainMetrics(temp: Double, pressure: Double, humidity: Double, temp_min: Double, temp_max: Double)

case class Wind(speed: Double, deg: Double)

case class Clouds(all: Int)

case class Rain(`3h`: Double)

case class Snow(`3h`: Double)

case class System(`type`: Int, id: Int, message: Double, country: String, sunrise: Long, sunset: Long)

case class City(id: Long, name: String)

// Current weather
case class WeatherCurrent(coord: Coordinates,
                          weather: List[Weather],
                          base: String,
                          main: MainMetrics,
                          visibility: Int,
                          wind: Option[Wind],
                          clouds: Option[Clouds],
                          rain: Option[Rain],
                          snow: Option[Snow],
                          dt: Long,
//                          sys: System,
                          id: Int,
                          name: String,
                          cod: Int)

// 5 Days forecast
case class Forecast(dt: Long, main: MainMetrics, weather: Seq[Weather], clouds: Option[Clouds], wind: Option[Wind])

case class WeatherForecast(city: City,
                           coord: Option[Coordinates],
                           country: Option[String],
                           cod: String,
                           message: Double,
                           cnt: Int,
                           list: Seq[Forecast])
