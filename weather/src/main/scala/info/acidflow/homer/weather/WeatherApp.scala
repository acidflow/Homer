package info.acidflow.homer.weather

import akka.actor.{ActorSystem, PoisonPill}
import akka.http.scaladsl.Http
import akka.stream.alpakka.mqtt.{MqttConnectionSettings, MqttQoS}
import akka.stream.scaladsl.{Keep, Sink}
import akka.stream.{ActorAttributes, ActorMaterializer}
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.LazyLogging
import info.acidflow.homer.SnipsApp
import info.acidflow.homer.dto.snips.SnipsMessage
import info.acidflow.homer.modules.TtsModule
import info.acidflow.homer.mqtt.MqttConfig
import info.acidflow.homer.stream.LogResumeDecider
import info.acidflow.homer.weather.api.OpenWeatherApi
import io.circe.generic.auto._
import io.circe.parser._
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object WeatherApp extends SnipsApp with TtsModule with LazyLogging {

  override lazy val config: Config = ConfigFactory.load()
  override lazy implicit val system: ActorSystem = ActorSystem("weather")
  override lazy implicit val executionContext: ExecutionContext = system.dispatcher
  override lazy implicit val materializer: ActorMaterializer = ActorMaterializer()

  lazy val mqttConfig = MqttConfig(config)
  override lazy val connectionSettings: MqttConnectionSettings = MqttConnectionSettings(mqttConfig.uri, "", new MemoryPersistence)
  override lazy val subscriptions = Map(
    "hermes/intent/searchWeatherForecast" -> MqttQoS.AtLeastOnce
  )

  val openWeatherApi = new OpenWeatherApi(
    Http(system),
    config.getString("weather.open_weather_map.base_url"),
    config.getString("weather.open_weather_map.api_key"),
    config.getString("weather.open_weather_map.units"),
    config.getString("weather.open_weather_map.default_city_id")
  )

  val weatherActor = system.actorOf(
    WeatherActor.props(ttsActor, openWeatherApi), "weather_handler"
  )

  mqttSource.map(msg => {
    val jsonStr = msg.payload.decodeString("UTF-8")
    logger.debug(jsonStr)

    decode[SnipsMessage](jsonStr).right.get
  }).toMat(Sink.actorRef(weatherActor, PoisonPill))(Keep.left)
    .withAttributes(ActorAttributes.supervisionStrategy(LogResumeDecider.decider))
    .run()
    .onComplete {
      case Success(_) =>
        logger.info("Actor {} subscribed to MQTT {} for events {}",
          weatherActor, mqttConfig.uri, subscriptions.keySet
        )
      case Failure(e) =>
        logger.error("Error whime connecting to MQTT bus", e)
    }

}
