package info.acidflow.homer.weather.api

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.concurrent.Executors

import akka.http.scaladsl.HttpExt
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.{HttpRequest, Uri}
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.LazyLogging
import info.acidflow.homer.weather.api.dto.{WeatherCurrent, WeatherForecast}
import info.acidflow.homer.weather.api.mapper.ForecastMappers._
import info.acidflow.homer.weather.api.mapper.HttpResponseMapper._
import info.acidflow.homer.weather.model.Forecast
import io.circe.generic.auto._

import scala.concurrent.{ExecutionContext, Future}

class OpenWeatherApi(val httpClient: HttpExt, baseUrl: String, apiKey: String, units: String, cityId: String)(implicit materializer: ActorMaterializer) extends LazyLogging {

  implicit val ec: ExecutionContext = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  val baseUri = Uri(baseUrl)
  val weatherUri = baseUri.copy(path = baseUri.path./("weather"))
  val forecastUri = baseUri.copy(path = baseUri.path./("forecast"))
  val defaultQuery = Query(("appid", apiKey), ("units", units))


  def forecast(cityName: Option[String], startDate: Option[LocalDateTime]): Future[Forecast] = {
    val today = LocalDateTime.now().toLocalDate
    if (startDate.exists(_.toLocalDate.isAfter(today))) {
      cityName.map(
        forecastWeatherCityName(_, startDate.get)
      ).getOrElse(
        forecastWeatherCity(startDate.get)
      )
    } else {
      cityName.map(
        currentWeatherCityName
      ).getOrElse(
        currentWeatherCity()
      )
    }
  }

  private def currentWeatherCity() = {
    val query = defaultQuery.+:(("id", cityId))
    val request = weatherUri.copy(rawQueryString = Option(query.toString()))

    httpClient.singleRequest(HttpRequest(uri = request))
      .flatMap(_.parseTo[WeatherCurrent])
      .map(toForecast)
  }

  private def currentWeatherCityName(cityName: String) = {
    val query = defaultQuery.+:(("q", cityName))
    val request = weatherUri.copy(rawQueryString = Option(query.toString()))

    httpClient.singleRequest(HttpRequest(uri = request))
      .flatMap(_.parseTo[WeatherCurrent])
      .map(toForecast)
  }

  private def forecastWeatherCity(date: LocalDateTime) = {
    require(ChronoUnit.DAYS.between(LocalDateTime.now(), date) < 5, "Cannot read weather forecast in more than 5 days.")

    val query = defaultQuery.+:(("id", cityId))
    val request = forecastUri.copy(rawQueryString = Option(query.toString()))

    httpClient.singleRequest(HttpRequest(uri = request))
      .flatMap(_.parseTo[WeatherForecast])
      .map(toForecast(_, date))
  }

  private def forecastWeatherCityName(cityName: String, date: LocalDateTime) = {
    require(ChronoUnit.DAYS.between(LocalDateTime.now(), date) < 5, "Cannot read weather forecast in more than 5 days.")

    val query = defaultQuery.+:(("q", cityName))
    val request = forecastUri.copy(rawQueryString = Option(query.toString()))

    httpClient.singleRequest(HttpRequest(uri = request))
      .flatMap(_.parseTo[WeatherForecast])
      .map(toForecast(_, date))
  }

}
