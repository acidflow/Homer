package info.acidflow.homer.weather.api.mapper

import java.time.{LocalDateTime, ZoneOffset}

import info.acidflow.homer.weather.api.dto.{WeatherCurrent, WeatherForecast}
import info.acidflow.homer.weather.model.Forecast

object ForecastMappers {

  def toForecast(forecast: WeatherForecast, dateTime: LocalDateTime): Forecast = {
    forecast.list.find(f =>
      LocalDateTime.ofEpochSecond(f.dt, 0, ZoneOffset.UTC).isAfter(dateTime)
    ).map(f =>
      Forecast(
        forecast.city.name,
        f.weather.head.description,
        f.main.temp,
        dateTime
      )
    ).getOrElse(throw new RuntimeException())
  }

  def toForecast(currentWeather: WeatherCurrent): Forecast = {
    Forecast(
      currentWeather.name,
      currentWeather.weather.head.description,
      currentWeather.main.temp,
      LocalDateTime.ofEpochSecond(currentWeather.dt, 0, ZoneOffset.UTC)
    )
  }
}
