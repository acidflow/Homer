package info.acidflow.homer.weather.api.mapper

import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.stream.ActorMaterializer
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import io.circe.Decoder
import io.circe.generic.auto._
import io.circe.parser.decode

import scala.concurrent.{ExecutionContext, Future}

object HttpResponseMapper extends LazyLogging {

  implicit class Model(val response: HttpResponse) extends AnyVal {

    def parseTo[T : Decoder](implicit ec : ExecutionContext, materializer : ActorMaterializer): Future[T] = response match {
      case HttpResponse(StatusCodes.OK, _, entity, _) =>
        entity.dataBytes.runFold(ByteString(""))(_ ++ _).map { body =>
          val jsonStr = body.utf8String
          logger.info("Response : {}", jsonStr)

          val decoded = decode[T](jsonStr)

          decoded.right.get
        }
      case HttpResponse(_, _, _, _) =>
        throw new RuntimeException(s"$response")
    }
  }

}
