package info.acidflow.homer.weather.model.dto

object Slots {

  val forecastLocality = "forecast_locality"
  val forecastStartDatetime = "forecast_start_datetime"

}
