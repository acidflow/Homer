import sbt._

object Dependencies {

  object Versions {
    val Circe = "0.9.3"
    val Alpakka = "0.19"
    val AkkaSlf4J = "2.5.13"
    val AkkaHttp = "10.1.1"
    val ScalaLogging = "3.9.0"
    val LogBack = "1.2.3"
    val ScalaTest = "3.0.5"
  }

  object Modules {

    val Core = Seq(
      "io.circe" %% "circe-core" % Versions.Circe,
      "io.circe" %% "circe-generic" % Versions.Circe,
      "io.circe" %% "circe-parser" % Versions.Circe,
      "io.circe" %% "circe-java8" % Versions.Circe,
      "com.lightbend.akka" %% "akka-stream-alpakka-mqtt" % Versions.Alpakka,
      "com.typesafe.scala-logging" %% "scala-logging" % Versions.ScalaLogging,
      "com.typesafe.akka" %% "akka-slf4j" % Versions.AkkaSlf4J,

      "org.scalatest" %% "scalatest" % Versions.ScalaTest % Test
    )

    val Weather = Seq(
      "com.typesafe.akka" %% "akka-http" % Versions.AkkaHttp,
      "ch.qos.logback" % "logback-classic" % Versions.LogBack % Runtime,

      "org.scalatest" %% "scalatest" % Versions.ScalaTest % Test
    )

  }

}
