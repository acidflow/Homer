import sbt.Keys.version

val commonSettings = Seq(
  name := "HomerV2",
  version := "0.1",
  scalaVersion := "2.12.6"
)

lazy val root = (project in file("."))
    .settings(commonSettings)
  .aggregate(core, weather)


lazy val core = (project in file("core"))
  .settings(
    libraryDependencies ++= Dependencies.Modules.Core
  )

lazy val weather = (project in file("weather"))
  .dependsOn(core)
  .settings(
    libraryDependencies ++= Dependencies.Modules.Weather
  )

